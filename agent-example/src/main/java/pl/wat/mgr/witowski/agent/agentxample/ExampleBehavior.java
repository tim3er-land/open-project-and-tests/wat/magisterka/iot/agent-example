package pl.wat.mgr.witowski.agent.agentxample;

import jade.core.Agent;
import jade.core.behaviours.WakerBehaviour;
import pl.wat.mgr.witowski.agent.mgriotcore.behaviors.SendDataBehaviour;

import java.util.HashMap;
import java.util.Random;
import java.util.logging.Logger;

public class ExampleBehavior extends WakerBehaviour {
    private static final Logger LOGGER = Logger.getLogger(ExampleBehavior.class.getName());

    private final Random random;

    public ExampleBehavior(Agent a, long period) {
        super(a, period);
        random = new Random();
    }


    @Override
    protected void onWake() {
        LOGGER.info("wake up behavior");
        HashMap<String, Object> objectObjectHashMap = new HashMap<>();
        objectObjectHashMap.put("temperature", random.nextDouble());
        objectObjectHashMap.put("humidity", random.nextDouble());
        objectObjectHashMap.put("pressure", random.nextDouble());
        if (random.nextBoolean()) {
            myAgent.addBehaviour(new SendDataBehaviour(myAgent,  "/home/sensors", objectObjectHashMap));
        } else {
            myAgent.addBehaviour(new SendDataBehaviour(myAgent, "/home/garage", objectObjectHashMap));
        }
        myAgent.addBehaviour(new ExampleBehavior(myAgent, Math.abs(random.nextLong() % 60000)));
    }
}
