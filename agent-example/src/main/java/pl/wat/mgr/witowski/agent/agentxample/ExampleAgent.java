package pl.wat.mgr.witowski.agent.agentxample;

import pl.wat.mgr.witowski.agent.mgriotcore.agent.AbstrackAgent;

import java.util.Arrays;
import java.util.logging.Logger;

public class ExampleAgent extends AbstrackAgent {
    private static final Logger LOGGER = Logger.getLogger(ExampleAgent.class.getName());

    public ExampleAgent() {
        super( Arrays.asList("/home/door/#"));
    }

    @Override
    protected void setup() {
        super.setup();
        LOGGER.info("Setup ExampleAgent");
        addBehaviour(new ExampleBehavior(this, 1));
    }

    @Override
    public void onMessage(String message) {
        super.onMessage(message);
    }
}
